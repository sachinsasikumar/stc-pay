(function($) {

  $(function() {
    navigation();
    scrollEvents();
    animation();
    slickSlide();
    isotopCustom();
    selectPicker();
    tabSlider();
  });

  if (Modernizr.mq('(max-width: 991px)')) {
    $(window).scroll(function() {
      var scroll = $(window).scrollTop();
      if (scroll >= 90) {
        $(".global-head").addClass("sticky");
      } else {
        $(".global-head").removeClass("sticky");
      }
    });
  }

  function tabSlider() {
    if ($('.tab-wrapper .nav-tabs').length) {
      $('.tab-wrapper .nav-tabs').on('shown.bs.tab', function(ev) {
        $(this).find('a.nav-link').each(function() {
          if (this == ev.target) return;
          $(this).removeClass('active show');
        });
      });
      if (Modernizr.mq('(max-width: 1199px)')) {
        if ($('.nav-tabs').length) {
          $('.nav-tabs').slick({
            infinite: false,
            autoplay: false,
            autoplaySpeed: 2000,
            speed: 500,
            cssEase: 'linear',
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false
          });
          // if (!($('.has-slick.slick-slider .slick-slide').length > 2)) {
          //   $('.has-slick .slick-dots').hide();
          // }
        }
      }
    }
  }

  function selectPicker() {
    if ($('.filters-select').length) {
      $('.filters-select').selectpicker();
      $('.sm-btn').click(function() {
        // alert('fff');
        // $('.grid').children().show();
      });
    }
  }
  var $container = undefined;

  function isotopCustom() {
    if ($('.offer-list-wrapper').length) {
      // init Isotope
      var $grid = $('.grid').isotope({
        itemSelector: '.element-item',
        layoutMode: 'fitRows'
      });
      $container = $grid;
      // filter functions
      var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
          var number = $(this).find('.number').text();
          return parseInt(number, 10) > 50;
        },
        // show if name ends with -ium
        ium: function() {
          var name = $(this).find('.name').text();
          return name.match(/ium$/);
        }
      };
      // bind filter on select change
      $('.filters-select').on('changed.bs.select', function(e) {
        // get filter value from option value
        // var filterValue = this.value;
        var filterValue = $(e.currentTarget).val();
        // use filterFn if matches value
        filterValue = filterFns[filterValue] || filterValue;
        var initShow = 6;
        $grid.isotope({
          filter: filterValue
        });
        loadMore(initShow);
      });

      
      //****************************
      // Isotope Load more button
      //****************************
      var initShow = 6; //number of images loaded on init & onclick load more button
      var counter = initShow; //counter for load more button
      var iso = $container.data('isotope'); // get Isotope instance

      loadMore(initShow); //execute function onload

      function loadMore(toShow) {
        $container.find(".hidden").removeClass("hidden");

        var hiddenElems = iso.filteredItems.slice(toShow, iso.filteredItems.length).map(function(item) {
          return item.element;
        });
        $(hiddenElems).addClass('hidden');
        $container.isotope('layout');

        //when no more to load, hide show more button
        if (hiddenElems.length == 0) {
          $("#load-more").hide();
        } else {
          $("#load-more").show();
        };
      }

      //append load more button
      $container.after('<div class="sm-holder mt-4"><button id="load-more" class="btn btn btn-style-1 margin-top-5">See More</button></div>');

      //when load more button clicked
      $("#load-more").click(function() {
        if ($('.filters-select').data('clicked')) {
          //when filter button clicked, set initial value for counter

          counter = initShow;
          j$('.filters-select').data('clicked', false);
        } else {
          counter = counter;
        };
        counter = counter + initShow;

        loadMore(counter);
      });
    }
  }

  function navigation() {
    $('.nave-wrapper ul > li').click(function() {
      if ($(this).hasClass('has-sub')) {
        $(this).addClass('active');
        $(this).removeClass('current');
        $('.submenu-wrapper').slideDown();
      } else {
        $('.nave-wrapper ul > li').removeClass('active');
        $('.submenu-wrapper').slideUp();
      }
    });
    $('.mobile-menu-btn .menu-btn').click(function() {
      if ($(this).hasClass('close-btn')) {
        $(this).removeClass('close-btn');
        $('.mobile-menu').hide();
      } else {
        $(this).addClass('close-btn');
        $('.mobile-menu').show();
      }
    });
    $('.mobile-menu ul > li.has-sub').click(function() {
      if ($(this).hasClass('current')) {
        $(this).removeClass('current');
        $(this).find('ul').slideUp();
      } else {
        $('.mobile-menu li').removeClass('current').children('ul').slideUp();
        $(this).addClass('current');
        $(this).find('ul').slideDown();
      }
    });

    $("body").click(function() {
      $('.submenu-wrapper').slideUp();
      $('.nave-wrapper ul > li').removeClass('active');
    });

    $(".submenu-wrapper, .nave-wrapper ul > li").click(function(e) {
      e.stopPropagation();
    });
  }

  function slickSlide() {
    if (Modernizr.mq('(max-width: 767px)')) {
      var isMulti = $('.home-content-slide .lt-content').length > 2 ? true : false;
      if ($('.home-content-slide').length && $('.home-content-slide .lt-content').length > 2) {
        $('.home-content-slide').slick({
          speed: 600,
          cssEase: 'ease',
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: false,
          dots: true,
          customPaging: function(slider, i) {
            var thumb = $(slider.$slides[i]).data();
            return '<a class="dot">0' + (i + 1) + '</a>';
          },
          focusOnSelect: true,
          arrows: false
        });
      }
    }
  }


  $.fn.isInViewport = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + ($(this).outerHeight());

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    // return elementBottom > viewportTop && elementTop < viewportBottom;
    return elementBottom > viewportTop && elementTop < viewportBottom && elementTop > viewportTop;
  };

  function scrollEvents() {
    var lastScrollTop = 0;
    $(window).scroll(function() {
      if (Modernizr.mq('(min-width: 1200px)')) {
        var scrollHeight = $(window).scrollTop();
        if ($('.block2-wrapper').length) {

          var blockTop1 = $('.block2-wrapper').offset().top - 40;
          var blockTop2 = $('.block5-wrapper').offset().top;
        }
        if (scrollHeight > blockTop2) {
          $('.item-fixed-block').removeClass('fixed-block');
          $('.item-fixed-block').addClass('to-bottom');
        }
        if (scrollHeight > blockTop1 && scrollHeight <= blockTop2) {
          $('.item-fixed-block').addClass('fixed-block').removeClass('to-bottom');

        } else {
          $('.item-fixed-block').removeClass('fixed-block');
        }
        $('.scroll-block').each(function() {
          if ($(this).isInViewport()) {
            if (!$(this).hasClass('in-view')) {
              $(this).addClass('in-view').removeClass('off-view');
              var image = $(this).attr('data-image');
              var icon = $(this).attr('data-icon');
              var color = $(this).attr('data-color');
              $('.item-fixed-block .mobile-screen-wrap img').attr({
                'src': image
              });
              $('.item-fixed-block .color-block-bg').css({
                'background-color': color
              });
              console.log(icon);

              $('.item-fixed-block .icon-wrapper img').css({
                'opacity': 0
              });
              $('.item-fixed-block .icon-wrapper img[data-icon-num ="' + icon + '"]').css({
                'opacity': 1
              });
              // $('.item-fixed-block .icon-wrapper img').hide();
              // $('.item-fixed-block .icon-wrapper img[data-icon-num ="' + icon +'"]').fadeIn(200);
            }
          } else {
            $(this).addClass('off-view').removeClass('in-view')
          }
        });
      }
    })
  }

  function animation() {
    setTimeout(function() {
      AOS.init({
        once: true,
        duration: 900,
      });
    });
  }


})(jQuery);
