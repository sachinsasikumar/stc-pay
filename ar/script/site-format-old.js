jQuery.support.placeholder = (function(){
    var i = document.createElement('input');
    return 'placeholder' in i;
})();

var Site = {
	init: function() {
		this
			
			//._googleMap()
			
			._iePlaceholder();
			
	},
	
	_iePlaceholder: function(){
		if(!$.support.placeholder) {
			$('[placeholder]').focus(function() {
			  var input = $(this);
			  if (input.val() == input.attr('placeholder')) {
				input.val('');
				input.removeClass('placeholder');
			  }
			}).blur(function() {
			  var input = $(this);
			  if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
			  }
			}).blur();	
		}
		return this;
	},
	
};
$(function() {
	Site.init();
});
